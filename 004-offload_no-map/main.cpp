#include <openmp_test.h>
#include <cstdio>
#include <cstdint>


extern "C" 
void openmp_test_offload_no_map(uint8_t* __restrict__ ptr, size_t size);


int main()
{
  constexpr size_t size = BUFFER_SIZE;
  uint8_t array[size] = {0};

  printf(">>> openmp_test_offload_no_map\n");

  struct timespec tp0, tp1;
  openmp_gettime(tp0);
  openmp_test_offload_no_map(array, size);
  openmp_gettime(tp1);

  printf("--- openmp_test_offload_no_map  -> tdiff=%lf ms \n", openmp_timediff(tp0, tp1));
  printf("<<< openmp_test_offload_no_map\n");

  return 0;
}
