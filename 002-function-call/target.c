#pragma omp declare target
#include <omp.h>
#include <ti_omp_device.h>
extern int printf(const char *_format, ...);
#pragma omp end declare target


#pragma omp declare target
void openmp_test_empty_function(char* __restrict__ p, size_t bytes)
{
}
#pragma omp end declare target


void openmp_test_function_call(char* __restrict__ p, size_t bytes)
{
#pragma omp target map(to: bytes, p[0:bytes])
  {
    openmp_test_empty_function(p, bytes);
  }
}
