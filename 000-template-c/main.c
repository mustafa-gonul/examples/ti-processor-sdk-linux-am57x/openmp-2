#include <openmp_test.h>
#include <stdio.h>
#include <stdint.h>


void openmp_test_template_c(uint8_t* __restrict__ ptr, size_t size);


int main()
{
  static const size_t size = BUFFER_SIZE;
  uint8_t array[BUFFER_SIZE] = {0};

  struct timespec tp0, tp1;

  printf(">>> openmp_test_template_c\n");

  openmp_gettime(&tp0);
  openmp_test_template_c(array, size);
  openmp_gettime(&tp1);

  printf("--- openmp_test_template_c  -> tdiff=%lf ms \n", openmp_timediff(tp0, tp1));
  printf("<<< openmp_test_template_c\n");

  return 0;
}
