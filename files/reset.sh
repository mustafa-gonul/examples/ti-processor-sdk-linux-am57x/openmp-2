#!/bin/bash

# Reloads DSP monitor on host system after critical failure
# in DSP code like segmentation violation.

LOG_FILE=$HOME/omap-rproc.log

cd /sys/bus/platform/drivers/omap-rproc

echo 40800000.dsp > unbind
echo 41000000.dsp > unbind

ti-mct-heap-check -c
pkill ti-mctd

modprobe -r cmemk >  ${LOG_FILE} 2>&1
modprobe    cmemk >> ${LOG_FILE} 2>&1

echo 40800000.dsp > bind
echo 41000000.dsp > bind

/usr/bin/ti-mctd  >> ${LOG_FILE} 2>&1 &
