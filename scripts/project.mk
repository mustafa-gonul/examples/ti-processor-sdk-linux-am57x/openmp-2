# EXTRA_OBJS
# Extra object files to link the target
#
# EXTRA_FILES_TO_CLEAN
# Intermediate files created during the build process. These files should be removed while cleaning.
#
# EXCLUDE_C_SRC
# The file needed to be excluded from build
#
# EXCLUDE_CPP_SRC
# The file needed to be excluded from build
#
# CFLAGS
# Extra flags to give to the C compiler.
#
# CXXFLAGS
# Extra flags to give to the C++ compiler.
#
# LDFLAGS
# Extra flags to give to compilers when they are supposed to invoke the linker, ‘ld’, such as -L.
# Libraries (-lfoo) should be added to the LDLIBS variable instead.
#
# LDLIBS
# Library flags or names given to compilers when they are supposed to invoke the linker, ‘ld’.
# LOADLIBES is a deprecated (but still supported) alternative to LDLIBS.
# Non-library linker flags, such as -L, should go in the LDFLAGS variable.


# TI OpenMP
TARGET_CODE?=target.c

# Common Options
COMMON_FLAGS:=-O3 -I../include/
COMMON_LIBS:=

# TI Toolchain options
# OA_TC_OPTS:=$(COMMON_FLAGS) -I$${TARGET_ROOTDIR}usr/share/ti/cgt-c6x/include/
OA_TC_OPTS:=$(COMMON_FLAGS) -I$${TARGET_ROOTDIR}usr/share/ti/opencl/
OA_HC_OPTS:=$(COMMON_FLAGS) $(COMMON_LIBS) -fopenmp
# OA_TL_OPTS:=-lm
# OA_HL_OPTS:=-lm
OA_SHELL_OPTS:=--hc="$(OA_HC_OPTS)" --tc="$(OA_TC_OPTS)" #--hl="$(OA_HL_OPTS)" --tl="$(OA_TL_OPTS)"
# OA_SHELL_OPTS:=--show_cc_opts --verbose --hc="$(OA_HC_OPTS)" --tc="$(OA_TC_OPTS)" #--hl="$(OA_HL_OPTS)" --tl="$(OA_TL_OPTS)"
OA_SHELL_TMP_FILES:= *.out __TI_CLACC_KERNEL.c *.cl *.asm *.dsp_h *.bc *.objc *.if *.map *.opt *.int.c *.o *.obj __s2s_predef.gcc_h

# Default parameters
EXTRA_OBJS := $(TARGET_CODE)
EXTRA_FILES_TO_CLEAN := $(OA_SHELL_TMP_FILES)
EXCLUDE_C_SRC := $(TARGET_CODE)
EXCLUDE_CPP_SRC := $(TARGET_CODE)

# Flags
CFLAGS := $(COMMON_FLAGS)
CXXFLAGS := $(COMMON_FLAGS)
LDFLAGS := $(OA_SHELL_OPTS)
LDLIBS :=