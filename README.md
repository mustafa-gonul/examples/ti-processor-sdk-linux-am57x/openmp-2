# OpenMP Examples

This repository is created to test the OpenMP feature of TI Processor SDK. There can be something broken on not properly implemented in the OpenMP Runtime. Besides another target is to measure the performance, how much time spend while using OpenMP. I am using/running these examples on Beagleboard-X15.

## How to configure the project

## Examples

### 000-template-c

This is template project for C Programming Language.

#### 000-template-c Code

![000-template-c code](./000-template-c/images/code.png)

#### 000-template-c Result

![000-template-c result](./000-template-c/images/result.png)

### 000-template-cpp

This is template project for C++ Programming Language.

#### 000-template-cpp Code

![000-template-cpp code](./000-template-cpp/images/code.png)

#### 000-template-cpp Result

![000-template-cpp result](./000-template-cpp/images/result.png)

### 001-printf-debug

The example tests if the **printf** function and the basic features of OpenMP.

#### 001-printf-debug Code

![001-printf-debug code](./001-printf-debug/images/code.png)

#### 001-printf-debug Result

![001-printf-debug result](./001-printf-debug/images/result.png)

### 002-function-call

The example tests if a function, in which variables mapped to the target, can call another function with these mapped variables.

#### 002-function-call Code

![002-function-call code](./002-function-call/images/code.png)

#### 002-function-call Result

![002-function-call result](./002-function-call/images/result.png)

### 003-offload_map

The example tests how much time spent while mapping an array to the target.

#### 003-offload_map Code

![003-offload_map code](./003-offload_map/images/code.png)

#### 003-offload_map Result

![003-offload_map result](./003-offload_map/images/result.png)

### 004-offload_no-map

The example tests how much time spent while offloading and there is no variable to map to the target.

#### 004-offload_no-map Code

![004-offload_no-map code](./004-offload_no-map/images/code.png)

#### 004-offload_no-map Result

![004-offload_no-map result](./004-offload_no-map/images/result.png)

### 005-offload_cmem

The example tests how much time spent while mapping an array to the target via Contiguous Memory (cmem).

#### 005-offload_cmem Code

![005-offload_cmem code](./005-offload_cmem/images/code.png)

#### 005-offload_cmem Result

![005-offload_cmem result](./005-offload_cmem/images/result.png)

### 006-offload_cmem-in

The example tests how much time spent while mapping an array **to the target** via Contiguous Memory (cmem).

#### 006-offload_cmem-in Code

![006-offload_cmem-in code](./006-offload_cmem-in/images/code.png)

#### 006-offload_cmem-in Result

![006-offload_cmem-in result](./006-offload_cmem-in/images/result.png)

### 007-offload_cmem-out

The example tests how much time spent while mapping an array **from the target** via Contiguous Memory (cmem).

#### 007-offload_cmem-out Code

![007-offload_cmem-out code](./007-offload_cmem-out/images/code.png)

#### 007-offload_cmem-out Result

![007-offload_cmem-out result](./007-offload_cmem-out/images/result.png)

### 008-offload_cmem-both

The example tests how much time spent while mapping an array **both to and from the target** via Contiguous Memory (cmem).

#### 008-offload_cmem-both Code

![008-offload_cmem-both code](./008-offload_cmem-both/images/code.png)

#### 008-offload_cmem-out Result

![007-offload_cmem-both result](./008-offload_cmem-both/images/result.png)
