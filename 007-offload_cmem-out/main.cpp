#include <openmp_test.h>
#include <ti_heap.h>
#include <ti_omp_device.h>
#include <cstdio>
#include <cstdint>


extern "C" 
void openmp_test_offload_cmem_out(uint8_t* __restrict__ ptr, size_t size);


int main()
{
  /*------------------------------------------------------------------------
  * From the host, create the underlying memory store for the heaps       
  *-----------------------------------------------------------------------*/
  constexpr size_t ddr_heap_size  = 16 << 20; //16MB
  constexpr size_t msmc_heap_size = 1 << 17;  //128KB
  constexpr size_t l2_heap_size = 1 << 10;    //1KB

  auto HeapDDR = (uint8_t*) __malloc_ddr(ddr_heap_size);
  auto HeapMSMC = (uint8_t*) __malloc_msmc(msmc_heap_size);

  /*------------------------------------------------------------------------
  * Initialize the pre-allocated buffers as new DDR and MSMC heaps 
  * accesible to DSP cores.                                              
  *-----------------------------------------------------------------------*/
  ti_heap_init_ddr(HeapDDR,  ddr_heap_size);
  ti_heap_init_msmc(HeapMSMC, msmc_heap_size);

  constexpr size_t size = BUFFER_SIZE;
  auto array = (uint8_t*) __malloc_ddr(size);

  printf(">>> openmp_test_offload_cmem_out\n");

  struct timespec tp0, tp1;
  openmp_gettime(tp0);
  openmp_test_offload_cmem_out(array, size);
  openmp_gettime(tp1);

  printf("--- openmp_test_offload_cmem_out  -> tdiff=%lf ms \n", openmp_timediff(tp0, tp1));
  printf("<<< openmp_test_offload_cmem_out\n");

  return 0;
}
