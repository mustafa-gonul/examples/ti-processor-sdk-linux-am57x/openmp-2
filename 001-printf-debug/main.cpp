#include <openmp_test.h>
#include <cstdio>
#include <limits>


extern "C" 
void openmp_test_printf_debug(int num_procs_to_set);


int main()
{
  int num_procs_to_set = 4;

  printf(">>> openmp_test_printf_debug\n");

  struct timespec tp0, tp1;
  openmp_gettime(tp0);
  openmp_test_printf_debug(num_procs_to_set);
  openmp_gettime(tp1);

  printf("--- openmp_test_printf_debug  -> tdiff=%lf ms \n", openmp_timediff(tp0, tp1));
  printf("<<< openmp_test_printf_debug\n");
  

  return 0;
}
