#pragma omp declare target
#include <omp.h>
#include <ti_omp_device.h>
extern int printf(const char *_format, ...);
#pragma omp end declare target


void openmp_test_printf_debug(int num_procs_to_set)
{
#pragma omp target map(to:num_procs_to_set)
  {
    printf("Number of Processor Cores: %d\n", omp_get_num_procs());
    printf("Maximum number of OpenMP threads: %d\n", omp_get_max_threads());
    printf("Starting 1st parallel region..\n");

    #pragma omp parallel
    {
      #pragma omp master
      {
        printf("\tNumber of OpenMP threads in team: %d\n", omp_get_num_threads());
      }

      printf("\tHello from thread: %d\n", omp_get_thread_num());
    }

    printf("Setting number of OpenMP threads to use to %d\n", num_procs_to_set);
    omp_set_num_threads(num_procs_to_set);
    printf("Starting 2nd parallel region..\n");

    #pragma omp parallel
    {
      #pragma omp master
      {
        printf("\tNumber of OpenMP threads in team: %d\n",  omp_get_num_threads());
      }
      printf("\tHello from thread: %d\n", omp_get_thread_num());
    }
  }
}
