#include <ti_heap.c>

#pragma omp declare target
#include <omp.h>
#include <ti_omp_device.h>
extern int printf(const char *_format, ...);
#pragma omp end declare target


void openmp_test_offload_cmem_in(uint8_t* __restrict__ ptr, size_t size)
{
#pragma omp target map(to: size, ptr[0:size])
  {
  }
}
