#pragma once

#define BUFFER_SIZE (1024 * 1024 * 6)
// #define BUFFER_SIZE (1024)
#define WIDTH_SIZE  (2048)
#define HEIGHT_SIZE (2048)
#define TEST_COUNT  (20)
