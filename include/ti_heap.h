#pragma once

#include <stddef.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


void ti_heap_init_ddr(uint8_t* __restrict__ ptr, size_t size);
void ti_heap_init_msmc(uint8_t* __restrict__ ptr, size_t size);
void ti_heap_init_L2(uint8_t* __restrict__ ptr, size_t size);


#ifdef __cplusplus
}
#endif
