#pragma once

int _add2(int v1, int v2);
int _add4 (int v1, int v2);
unsigned _packl4(unsigned a, unsigned b);
unsigned _packh4(unsigned a, unsigned b);
unsigned _pack2(unsigned a, unsigned b);
unsigned _packh2(unsigned a, unsigned b);
unsigned _spacku4(int b, int a);
int _spack2 (int s1, int s2);
unsigned _unpklu4 (unsigned src);
unsigned _unpkhu4 (unsigned src);
// unsigned short _mem2(void *ptr); // declaration hides built-in function
// unsigned _amem4(void *ptr); // declaration hides built-in function
// unsigned _amem4_const(const void *ptr); // declaration hides built-in function
// unsigned _mem4_const(const void *ptr); // declaration hides built-in function
// unsigned _mem4(void *ptr); // declaration hides built-in function
// double _memd8(void *ptr); // declaration hides built-in function
// double _amemd8(void *ptr); // declaration hides built-in function
// double _amemd8_const(const void *ptr); // declaration hides built-in function
// double _mem8_const(const void *ptr); // declaration hides built-in function
double _itod(unsigned v2, unsigned v1);
long long _itoll (unsigned v2, unsigned v1);
unsigned _hi(double d);
unsigned _lo(double d);
unsigned _hill (long long src);
unsigned _loll (long long src);
int _max2(int a, int b);
int _min2(int a, int b);
int _sub2(int a, int b);
int _cmpgt2(int a, int b);
unsigned _maxu4(unsigned a, unsigned b);
unsigned _minu4(unsigned a, unsigned b);
unsigned _swap4(unsigned a);
unsigned _packhl2(unsigned a, unsigned b);
unsigned _packlh2(unsigned a, unsigned b);
// unsigned short _mem2_const(const void *ptr); // declaration hides built-in function
unsigned _shrmb (unsigned src1, unsigned src2);
unsigned _shlmb (unsigned src1, unsigned src2);
unsigned _lmbd(unsigned src2, unsigned src1);
int _dotpsu4 (int src1, unsigned src2);
// int _dotprsu2(int v1, unsigned v2);
int _dotpnrsu2 (int v1, unsigned v2);
int _dotp2 (int v1, int v2);
long long _ddotp4 (unsigned src1, unsigned src2);
int _abs (int src1);
long long _mpy32u(unsigned src1, unsigned src2);
double _mpyu4 (unsigned src1, unsigned src2);
long long _mpy32ll (int src1, int src2);
unsigned _bitc4(unsigned val);
unsigned _dotpu4(unsigned v1, unsigned v2);
unsigned _bitr(unsigned v1);
unsigned _cmpgtu4(unsigned v1, unsigned v2);
unsigned _xpnd4 (unsigned src);
unsigned _saddu4 (unsigned src1, unsigned src2);
// long long _addsub2(int src1, int src2); // declaration hides built-in function
unsigned _rotl_dsp (unsigned v2, unsigned v1);
unsigned _avgu4(unsigned v2, unsigned v1);
unsigned _set(unsigned src2, unsigned csta, unsigned cstb);
unsigned _shfl(unsigned src);
unsigned _extu (unsigned src2, unsigned csta, unsigned cstb);
unsigned _deal (unsigned src);
