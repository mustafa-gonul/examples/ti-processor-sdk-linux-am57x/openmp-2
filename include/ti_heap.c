#pragma once

/*-----------------------------------------------------------------------------
* User-controlled DSP heaps are initialized within a target region. The call
* to __heap_init_xxx can be included within any target region. However the
* initialization function must be called before any __malloc_xxx calls are
* made.
*
* User-controlled DSP heaps can be persistent across target regions as long as
* the underlying memory (aka buffers pointed to by p are not deallocated.
*----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stddef.h>


#pragma omp declare target
#include <omp.h>
#include <ti_omp_device.h>
extern int printf(const char *_format, ...);
#pragma omp end declare target


void ti_heap_init_ddr(uint8_t* __restrict__ ptr, size_t size)
{
#pragma omp target map(to:size, ptr[0:size])
  {
    printf("ti_heap_init_ddr\n");
     __heap_init_ddr(ptr, size);
  }
}


void ti_heap_init_msmc(uint8_t* __restrict__ ptr, size_t size)
{
#pragma omp target map(to: size, ptr[0:size])
  {
    printf("ti_heap_init_msmc\n");
    __heap_init_msmc(ptr, size);
  }
}
