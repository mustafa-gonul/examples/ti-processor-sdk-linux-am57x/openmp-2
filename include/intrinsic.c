#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int _add2(int v1, int v2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _add4 (int v1, int v2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _packl4(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _packh4(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _pack2(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _packh2(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _spacku4(int b, int a)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _spack2 (int s1, int s2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _unpklu4 (unsigned src)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _unpkhu4 (unsigned src)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned short _mem2(void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _amem4(void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _amem4_const(const void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _mem4_const(const void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _mem4(void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

double _memd8(void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

double _amemd8(void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

double _amemd8_const(const void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

double _mem8_const(const void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

double _itod(unsigned v2, unsigned v1)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

long long _itoll (unsigned v2, unsigned v1)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _hi(double d)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _lo(double d)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _hill (long long src)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _loll (long long src)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _max2(int a, int b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _min2(int a, int b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _sub2(int a, int b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _cmpgt2(int a, int b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _maxu4(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _minu4(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _swap4(unsigned a)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _packhl2(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _packlh2(unsigned a, unsigned b)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned short _mem2_const(const void *ptr)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _shrmb (unsigned src1, unsigned src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _shlmb (unsigned src1, unsigned src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _lmbd(unsigned src2, unsigned src1)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _dotpsu4 (int src1, unsigned src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _dotpnrsu2 (int v1, unsigned v2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _dotp2 (int v1, int v2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

long long _ddotp4 (unsigned src1, unsigned src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

int _abs (int src1)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

long long _mpy32u(unsigned src1, unsigned src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

double _mpyu4 (unsigned src1, unsigned src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

long long _mpy32ll (int src1, int src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _bitc4(unsigned val)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _dotpu4(unsigned v1, unsigned v2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _bitr(unsigned v1)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _cmpgtu4(unsigned v1, unsigned v2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _xpnd4 (unsigned src)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _saddu4 (unsigned src1, unsigned src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

long long _addsub2(int src1, int src2)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _rotl_dsp (unsigned v2, unsigned v1)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _avgu4(unsigned v2, unsigned v1)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _set(unsigned src2, unsigned csta, unsigned cstb)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _shfl(unsigned src)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _extu (unsigned src2, unsigned csta, unsigned cstb)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

unsigned _deal (unsigned src)
{
  printf("Error: OpenMP calling the ARM function! -> %s\n", __func__);

  return 0;
}

#ifdef __cplusplus
}
#endif
