#pragma once

#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif


void openmp_test(char* __restrict__ p, size_t bytes);
void openmp_test_2(char* __restrict__ in, char* __restrict__ out, size_t bytes);

void openmp_test_hello_world(char* __restrict__ p, size_t bytes);
void openmp_test_null(char* __restrict__ p, size_t bytes);
void openmp_test_null_map(char* __restrict__ p, size_t bytes);
void openmp_test_null_map_2(char* __restrict__ in, char* __restrict__ out, size_t bytes);
void openmp_test_assign_0(char* __restrict__ p, size_t bytes);
void openmp_test_assign_1(char* __restrict__ p, size_t bytes);
void openmp_test_copy(const char* __restrict__ in, char* __restrict__ out, size_t bytes);


#ifdef __cplusplus
}
#endif