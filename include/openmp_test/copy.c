#include <ti_heap.c>


void openmp_test_copy(const char* __restrict__ in, char* __restrict__ out, size_t bytes)
{
#pragma omp target map(to: bytes, in[0:bytes]) map(from: out[0:bytes])
  {
    size_t i = 0;
#pragma omp parallel for
    for (i = 0; i < bytes; ++i) {
      out[i] = in[i];
    }
  }
}
