#include <ti_heap.c>


void openmp_test_null(char* __restrict__ p, size_t bytes)
{
#pragma omp target // map(to: bytes, p[0:bytes])
  {
  }
}

void openmp_test_null_map(char* __restrict__ p, size_t bytes)
{
#pragma omp target map(to: bytes, p[0:bytes])
  {
  }
}

void openmp_test_null_map_2(char* __restrict__ in, char* __restrict__ out, size_t bytes)
{

}
