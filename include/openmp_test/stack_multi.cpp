#define STR1(x) #x
#define STR(x) STR1(x)


#ifndef TEST_FUNCTION
#define TEST_FUNCTION openmp_test
#pragma message "The default " STR(TEST_FUNCTION) " as TEST_FUNCTION is used!"
#endif


#include <openmp_test.h>
#include <stdio.h>


int main()
{
  char buffer[BUFFER_SIZE] = {0};
  size_t bufferSize = sizeof(buffer) / sizeof(buffer[0]);

  struct timespec tp0, tp1;

  for (int i = 0; i < TEST_COUNT; ++i) {
    printf("--- Test %d ---\n", i);

    openmp_gettime(tp0);
    TEST_FUNCTION(buffer, bufferSize);
    openmp_gettime(tp1);
#ifdef TEST_FUNCTION_2
    TEST_FUNCTION_2(buffer, bufferSize);
    openmp_gettime(tp2);
#endif

    printf("---\n");
    printf( STR(TEST_FUNCTION) " -> time diff =%lf ms \n", openmp_timediff(tp0, tp1));
#ifdef TEST_FUNCTION_2
    printf( STR(TEST_FUNCTION_2) " -> time diff =%lf ms \n", openmp_timediff(tp1, tp2));
#endif
    printf("---\n");
  }

  return 0;
}
