#define STR1(x) #x
#define STR(x) STR1(x)


#ifndef TEST_FUNCTION
#define TEST_FUNCTION openmp_test
#pragma message "The default " STR(TEST_FUNCTION) " as TEST_FUNCTION is used!"
#endif


#include <openmp_test.h>
#include <stdio.h>


int main()
{
  /*------------------------------------------------------------------------
  * From the host, create the underlying memory store for the heaps       
  *-----------------------------------------------------------------------*/
  int ddr_heap_size  = 16 << 20; //16MB
  int msmc_heap_size = 1 << 17;  //128KB
  int l2_heap_size = 1 << 10;    //1KB

  char* HeapDDR = (char*) __malloc_ddr(ddr_heap_size);
  char* HeapMSMC = (char*) __malloc_msmc(msmc_heap_size);

  /*------------------------------------------------------------------------
  * Initialize the pre-allocated buffers as new DDR and MSMC heaps 
  * accesible to DSP cores.                                              
  *-----------------------------------------------------------------------*/
  heap_init_ddr (HeapDDR,  ddr_heap_size);
  heap_init_msmc(HeapMSMC, msmc_heap_size);

  //
  size_t bufferSize = BUFFER_SIZE;
  char* buffer = (char*) __malloc_ddr(bufferSize);

  //
  struct timespec tp0, tp1, tp2;

  printf("---\n");

  openmp_gettime(tp0);
  TEST_FUNCTION(buffer, bufferSize);
  openmp_gettime(tp1);
#ifdef TEST_FUNCTION_2
  TEST_FUNCTION_2(buffer, bufferSize);
  openmp_gettime(tp2);
#endif


  printf("---\n");
  printf( STR(TEST_FUNCTION) " -> time diff =%lf ms \n", openmp_timediff(tp0, tp1));
#ifdef TEST_FUNCTION_2
  printf( STR(TEST_FUNCTION_2) " -> time diff =%lf ms \n", openmp_timediff(tp1, tp2));
#endif
  printf("---\n");

  return 0;
}
