#include <ti_heap.c>


void openmp_test_hello_world(char* __restrict__ p, size_t bytes)
{
  #pragma omp target map(to: bytes, p[0:bytes])
  {
    printf("Hello World!\n");
  }
}
