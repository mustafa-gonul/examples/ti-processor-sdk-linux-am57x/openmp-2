#include <ti_heap.c>


void openmp_test_assign_0(char* __restrict__ p, size_t bytes)
{
#pragma omp target map(to: bytes, p[0:bytes])
  {
    size_t i = 0;
#pragma omp parallel for
    for (i = 0; i < bytes; ++i) {
      p[i] = 0;
    }
  }
}

void openmp_test_assign_1(char* __restrict__ p, size_t bytes)
{
#pragma omp target map(to: bytes, p[0:bytes])
  {
    size_t i = 0;

#pragma omp parallel for
    for (i = 0; i < bytes; ++i) {
      p[i] = 1;
    }
  }
}
