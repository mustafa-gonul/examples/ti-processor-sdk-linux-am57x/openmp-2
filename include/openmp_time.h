#pragma once

#include <time.h>
#include <unistd.h>


#ifdef __cplusplus

/* Time difference calculation, in ms units */
inline double openmp_timediff(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec - tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

inline void openmp_gettime(struct timespec &tp)
{
  clock_gettime(CLOCK_MONOTONIC, &tp);
}

#else

/* Time difference calculation, in ms units */
inline double openmp_timediff(struct timespec tp_start, struct timespec tp_end)
{
  return (double)(tp_end.tv_nsec - tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

inline void openmp_gettime(struct timespec *tp)
{
  clock_gettime(CLOCK_MONOTONIC, tp);
}


#endif
